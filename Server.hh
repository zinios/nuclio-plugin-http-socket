<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\http\socket
{
	require_once('types.hh');
	
	use nuclio\core\ClassManager;
	
	<<factory>>
	class Server
	{
		private int $port					=DEFAULT_PORT;
		private bool $running				=false;
		private ClientCollection $clients	=Vector{};
		private array<mixed> $errors		=[null,null];
		private ?ServerClient $serverClient;
		
		public static function getInstance(/* HH_FIXME[4033] */...$args):Server
		{
			$instance=ClassManager::getClassInstance(self::class,...$args);
			return ($instance instanceof self)?$instance:new self(...$args);
		}
		
		public function __construct(int $port=DEFAULT_PORT)
		{
			$this->port=$port;
		}
		
		public function isRunning():bool
		{
			return $this->running;
		}
		
		public function start():this
		{
			if (!$this->running)
			{
				$this->consoleMessage('Hello. This is the Nuclio socket server.');
				$this->consoleMessage('Starting Server...');
				// $this->consoleMessage('Opening socket...');
				$this->running=true;
				// $serverSocket=stream_socket_server
				// (
				// 	'tcp://0.0.0.0:'.$this->port,
				// 	$this->errors[0],
				// 	$this->errors[1]
				// );
				$serverSocket=socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
				socket_bind($serverSocket, '0.0.0.0', $this->port);
				socket_listen($serverSocket);
				socket_set_nonblock($serverSocket);
				if (!$serverSocket)
				{
					throw new SocketException('Server was unable to start.');
				}
				$this->serverClient=ServerClient::getInstance($serverSocket);
				
				$this->clients[]=$this->serverClient;
				$this->consoleMessage('Server is ready to accept connections...');
				while (true)
				{
					$this->consoleMessage('Checking for changes...');
					if (!$this->running)
					{
						break;
					}
					$clients		=$this->clients;
					$write			=null;
					$except			=null;
					$connections	=$this->collectConnections();
					
					// var_dump($serverSocket);
					// $newClient=socket_accept($serverSocket);
					
					// if ($newClient!==false)
					// {
					// 	$this->consoleMessage('Client connected...');
						
					// 	exit();
					// }
					// $this->consoleMessage('No new clients...');
					
					// sleep(2);
					
					// continue;
					// $numChanged		=stream_select($connections,$write,$except,5);
					// $numChanged=socket_accept($serverSocket);
					// $connections	=$this->collectConnections();
					// var_dump($this->collectConnections());
					// if (count($connections)>1)
					// {
					// 	$xConnections	=[$connections[1]];
					// 	$numChanged		=stream_select($xConnections,$write,$except,5);
					// }
					// else
					// {
					// 	$xConnections	=[$connections[0]];
					// 	$numChanged		=stream_select($xConnections,$write,$except,5);
					// }
					
					$numChanged		=socket_select($connections,$write,$except,2);
					
					$this->consoleMessage('Num Changes: '.$numChanged);
					$this->consoleMessage('Num Clients: '.(count($clients)-1));
					// sort($clients);
					for ($i=0; $i<$numChanged; $i++)
					{
						if (get_resource_type($clients[$i]->getConnection())!=='stream')
						{
							//Client has disconnected.
							$this->consoleMessage('Client disconnected socket before I could fully establish connection.');
						}
						if ($clients[$i]===$this->serverClient)
						{$this->consoleMessage('x10');
							$serverClient=$this->serverClient;
							if (is_null($serverClient))
							{
								$this->consoleMessage('Lost the server client. Stopping service.');
								exit();
							}
							else
							{
								$connection=socket_accept($serverClient->getConnection());
								if ($connection)
								{
									
									// stream_set_blocking($connection,false);
									socket_set_nonblock($connection);
									$request=Request::getInstance($connection);
									// fwrite($connection, "Hello! The time is ".date("n/j/Y g:i a")."\n");
									$this->clients[]=Client::getInstance($request);
									$this->consoleMessage(sprintf('Client connected. Total clients is now %s.',count($this->clients)-1));
								}
								else
								{
									$this->consoleMessage('Client attempted connection but failed.');
									// $key=array_search($clients[$i],$this->clients,true);
									// fclose($clients[$i]->getConnection());
								}
							}
						}
						else
						{$this->consoleMessage('x20');
							$socketData=fread($clients[$i]->getConnection(),1024);
							
							if ($this->isConnectionClosed($clients[$i]))
							{$this->consoleMessage('x21');
								$key=array_search($clients[$i],$this->clients,true);
								fclose($clients[$i]->getConnection());
								$this->clients->removeKey($key);
								$this->consoleMessage(sprintf('Client disconnected (2). Total clients is now %s.',count($this->clients)-1));
							}
							else if ($socketData===false)
							{$this->consoleMessage('x22');
								$key=array_search($clients[$i],$this->clients,true);
								$this->clients->removeKey($key);
							}
							else
							{$this->consoleMessage('x23');
								print 'The client has sent: ';
								var_dump($socketData);
								fwrite($clients[$i],'You have sent :['.$socketData."]\n");
							}
						}
						// sort($this->clients);
					}
					// var_dump($this->clients);
					$this->consoleMessage('Waiting...');
					sleep(2);
				}
			}
			else
			{
				throw new SocketException('Unable to start because the stream has already been started.');
			}
			return $this;
		}
		
		public function stop():this
		{
			$this->running=false;
			
			foreach ($this->clients as $client)
			{
				fclose($client->getConnection());
			}
			$this->clients=Vector{};
			if (!is_null($this->serverClient))
			{
				fclose($this->serverClient->getConnection());
			}
			return $this;
		}
		
		private function isConnectionClosed(ClientType $client):bool
		{
			// $this->consoleMessage('isConnectionClosed');
			$socketData=fread($client->getConnection(),1024);
			return (strlen($socketData)===0);
		}
		
		private function consoleMessage(string $message):void
		{
			print "$message\n";
		}
		
		private function collectConnections():array<resource>
		{
			$connections=[];
			foreach ($this->clients as $client)
			{
				$connections[]=$client->getConnection();
			}
			// var_dump($connections);
			return $connections;
		}
		
		// private function reindex(ClientCollection $clients):ClientCollection
		// {
		// 	$newCollection=Vector{};
		// 	foreach ($clients as $client)
		// 	{
		// 		$newCollection[]=$client;
		// 	}
		// 	return $newCollection;
		// }
	}
}
