<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\http\socket
{
	use nuclio\core\ClassManager;
	
	<<factory>>
	class ServerClient implements ClientType
	{
		private resource $connection;
		
		public static function getInstance(/* HH_FIXME[4033] */...$args):ServerClient
		{
			$instance=ClassManager::getClassInstance(self::class,...$args);
			return ($instance instanceof self)?$instance:new self(...$args);
		}
		
		public function __construct(resource $connection)
		{
			$this->connection=$connection;
		}
		
		public function getConnection():resource
		{
			return $this->connection;
		}
	}
}
