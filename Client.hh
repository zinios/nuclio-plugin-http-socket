<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\http\socket
{
	use nuclio\core\ClassManager;
	
	<<factory>>
	class Client implements ClientType
	{
		private Request $request;
		private string $salt='258EAFA5-E914-47DA-95CA-C5AB0DC85B11';
		private ?string $key;
		
		public static function getInstance(/* HH_FIXME[4033] */...$args):Client
		{
			$instance=ClassManager::getClassInstance(self::class,...$args);
			return ($instance instanceof self)?$instance:new self(...$args);
		}
		
		public function __construct(Request $request)
		{
			$this->request	=$request;
			$this->key		=$this->request->getHeader('sec-websocket-key');
			if (!is_null($this->key))
			{
				var_dump($this->key);
				$this->respondWithHandshake();
			}
			else
			{
				print 'Invalid Web Socket Request! No security key provided.';
				// throw new SocketException('Invalid Web Socket Request! No security key provided.');
			}
		}
		
		public function getConnection():resource
		{
			return $this->request->getConnection();
		}
		
		private function respondWithHandshake():void
		{
			$responseKey=base64_encode(sha1($this->key.$this->salt,true));
			$message=<<<MESSAGE
HTTP/1.1 101 Switching Protocols
Upgrade: websocket
Connection: Upgrade
Sec-WebSocket-Accept: {$responseKey}


MESSAGE;
			$result=socket_write($this->getConnection(),$message,strlen($message));
			// fwrite($this->getConnection(),$message);
			// fclose($this->getConnection());
		}
	}
}
