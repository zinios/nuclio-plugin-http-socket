<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\http\socket
{
	use nuclio\core\ClassManager;
	
	<<factory>>
	class Emitter
	{
		private string $host	=DEFAULT_HOST;
		private int $port		=DEFAULT_PORT;
		private bool $running	=false;
		private ?resource $stream;
		
		public static function getInstance(/* HH_FIXME[4033] */...$args):Emitter
		{
			$instance=ClassManager::getClassInstance(self::class,...$args);
			return ($instance instanceof self)?$instance:new self(...$args);
		}
		
		public function __construct(string $host=DEFAULT_HOST,int $port=DEFAULT_PORT)
		{
			$this->host=$host;
			$this->port=$port;
			
		}
		
		public function isRunning():bool
		{
			return $this->running;
		}
		
		public function start():this
		{
			if (!$this->running)
			{
				$this->running=true;
				$errors=[null,null];
				// $this->stream=stream_socket_client
				// (
				// 	$this->host.':'.$this->port,
				// 	$errors[0],
				// 	$errors[1],
				// 	ini_get('default_socket_timeout'),
				// 	STREAM_CLIENT_CONNECT,
				// 	stream_context_create(null)
				// );
				// $this->stream=stream_socket_server
				// (
				// 	'tcp://'.$this->host.':'.$this->port,
				// 	$errors[0],
				// 	$errors[1],
				// 	ini_get('default_socket_timeout'),
				// 	STREAM_CLIENT_CONNECT,
				// 	stream_context_create(null)
				// );
			}
			else
			{
				throw new SocketException('Unable to start because the stream has already been started.');
			}
			return $this;
		}
		
		public function stop():this
		{
			$this->running=false;
			return $this;
		}
	}
}
