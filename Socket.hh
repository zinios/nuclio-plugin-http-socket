<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\http\socket
{
	require_once('types.hh');
	
	use nuclio\core\ClassManager;
	use nuclio\core\plugin\Plugin;
	
	<<singleton>>
	class Socket extends Plugin
	{
		private Map<int,Emitter> $emitters=Map{};
		
		
		public static function getInstance(/* HH_FIXME[4033] */...$args):Socket
		{
			$instance=ClassManager::getClassInstance(self::class);
			return ($instance instanceof self)?$instance:new self();
		}
		
		public function __construct()
		{
			parent::__construct();
		}
		
		public function createEmitter(string $host=DEFAULT_HOST,int $port=DEFAULT_PORT):Emitter
		{
			if (is_null($this->emitters->get($port)))
			{
				$emitter=new Emitter($host,$port);
			}
			else
			{
				throw new SocketException(sprintf('Unable to bind emitter to port "%s". Port already in use.',$port));
			}
			// $this->emitters->set($port,$emitter);
			return $emitter;
		}
	}
}
