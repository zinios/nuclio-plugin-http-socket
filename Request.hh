<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\http\socket
{
	require_once('types.hh');
	
	use nuclio\core\ClassManager;
	
	newtype HTTP_MAP	=Map<string,string>;
	
	<<factory>>
	class Request
	{
		private string $method;
		private HTTP_MAP $headers;
		private resource $connection;
		private Vector<string> $languages;
		
		public static function getInstance(/* HH_FIXME[4033] */...$args):Request
		{
			$instance=ClassManager::getClassInstance(self::class,...$args);
			return ($instance instanceof self)?$instance:new self(...$args);
		}
		
		public function __construct(resource $connection)
		{
			$this->connection	=$connection;
			$socketData			=fread($this->connection,1024);
			$this->method		=$this->parseMethod($socketData);
			$this->headers		=$this->mapHeaders($socketData);
			$this->languages	=$this->parseAcceptedLanguages($this->getHeader('accept-language'));
		}
		
		private function parseMethod(string $rawRequest):string
		{
			$lines	=explode("\n",$rawRequest);
			$method	=explode('/',array_shift($lines),2);
			
			return trim((string)$method[0]);
		}
		
		private function mapHeaders(string $rawHeaders):HTTP_MAP
		{
			$headers	=Map{};
			$lines		=explode("\n",$rawHeaders);
			array_shift($lines);
			foreach ($lines as $line)
			{
				$line=trim($line);
				if ($line==='')
				{
					continue;
				}
				list($key,$val)=explode(':',$line,2);
				$headers->set(strtolower(trim($key)),trim($val));
			}
			
			return $headers;
		}
		
		private function parseAcceptedLanguages(?string $languageHeader):Vector<string>
		{
			if (!is_null($languageHeader))
			{
				//Remove junk.
				$parts=explode(';',$languageHeader,2);
				//Return formatted langs.
				return new Vector(explode(',',$parts[0]));
			}
			return Vector{};
		}
		
		public function getConnection():resource
		{
			return $this->connection;
		}
		
		public function getHeaders():HTTP_MAP
		{
			return $this->headers;
		}
		
		public function getHeader(string $header):?string
		{
			return $this->headers->get($header);
		}
		
		public function getLanguages():Vector<string>
		{
			return $this->languages;
		}
	}
}
